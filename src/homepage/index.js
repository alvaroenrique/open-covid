import * as React from "react";

import "./style.scss";
import bg from "../images/bg-home.png";
import { dep, prov, dist } from "./utils";

export default function Homepage() {
  const [currentDep, setCurrentDep] = React.useState("");
  const [currentProv, setCurrentProv] = React.useState("");
  const [currentDist, setCurrentDist] = React.useState("");

  return (
    <div className="home__container">
      <button className="home__menu">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          width="53"
          height="23"
          viewBox="0 0 53 23"
        >
          <g transform="translate(-1728.5 -60.5)">
            <line
              x1="50"
              transform="translate(1730 62)"
              fill="none"
              stroke="#c51111"
              strokeLinecap="round"
              strokeWidth="3"
            />
            <line
              x1="50"
              transform="translate(1730 72)"
              fill="none"
              stroke="#c51111"
              strokeLinecap="round"
              strokeWidth="3"
            />
            <line
              x1="50"
              transform="translate(1730 82)"
              fill="none"
              stroke="#c51111"
              strokeLinecap="round"
              strokeWidth="3"
            />
          </g>
        </svg>
      </button>
      <img className="home__bg" src={bg} alt="Background logo"></img>
      <div className="text-white mr-14">
        <h1 className="flex flex-col items-end font-black">
          <div className="flex">
            <div
              style={{
                fontSize: "330px",
                color: "#C51111",
                lineHeight: "246px",
              }}
            >
              #
            </div>
            <div
              className="flex flex-col text-right"
              style={{ fontSize: "150px", lineHeight: "128px" }}
            >
              <span>YO</span>
              <span>ME</span>
            </div>
          </div>
          <div style={{ fontSize: "150px", lineHeight: "128px" }}>VACUNO</div>
        </h1>

        <h2
          className="text-right mt-8"
          style={{ fontSize: "35px", lineHeight: "40px" }}
        >
          <div>Registra tu vacunación hoy,</div>
          <div>y se parte del cambio</div>
        </h2>
      </div>
      <form className="home__form-container py-14  flex flex-col">
        <h2
          className="font-bold px-20"
          style={{
            fontSize: "50px",
            color: "#9D9D9D",
          }}
        >
          Ingresa
        </h2>
        <h3
          className="font-bold mt-2 mb-2 px-20"
          style={{
            fontSize: "20px",
            lineHeight: "25px",
            color: "#393939",
          }}
        >
          Si acabas de recibir la primera dosis de tu vacuna y estás a la espera
          de la segunda.
        </h3>
        <div className="px-20 pb-10 overflow-y-auto">
          <div className="home__input mt-5">
            <label htmlFor="dniInput">DNI</label>
            <input id="dniInput" type="text"></input>
          </div>
          <div className="home__input mt-5">
            <label htmlFor="dniDateInput">Fecha de emisión</label>
            <input id="dniDateInput" type="text"></input>
          </div>
          <div className="home__input mt-5">
            <label htmlFor="emailInput">Correo electrónico</label>
            <input id="emailInput" type="text"></input>
          </div>
          <div className="home__input mt-5">
            <label htmlFor="depInput">Departamento</label>
            <select
              id="depInput"
              name="depInput"
              value={currentDep}
              onChange={(ev) => {
                setCurrentDep(ev.target.value);
              }}
            >
              <option value></option>
              {dep.map(({ nombre_ubigeo, id_ubigeo }) => (
                <option key={id_ubigeo} value={id_ubigeo}>
                  {nombre_ubigeo}
                </option>
              ))}
            </select>
          </div>
          <div className="home__input mt-5">
            <label htmlFor="provInput">Provincia</label>
            <select
              id="provInput"
              name="provInput"
              value={currentProv}
              onChange={(ev) => {
                console.log(ev.target.value);
                setCurrentProv(ev.target.value);
              }}
            >
              <option value></option>
              {prov[currentDep]?.map(({ nombre_ubigeo, id_ubigeo }) => (
                <option key={id_ubigeo} value={id_ubigeo}>
                  {nombre_ubigeo}
                </option>
              ))}
            </select>
          </div>
          <div className="home__input mt-5">
            <label htmlFor="distInput">Distrito</label>
            <select
              id="distInput"
              name="distInput"
              value={currentDist}
              onChange={(ev) => {
                setCurrentDist(ev.target.value);
              }}
            >
              <option value></option>
              {dist[currentProv]?.map(({ nombre_ubigeo, id_ubigeo }) => (
                <option key={id_ubigeo} value={id_ubigeo}>
                  {nombre_ubigeo}
                </option>
              ))}
            </select>
          </div>
          <div className="home__input-check mt-5">
            <p htmlFor="gesInput">Género</p>
            <div>
              <input type="radio" id="masculino1" name="gen" />
              <label for="masculino1">Masculino</label>
              <input type="radio" id="femenino1" name="gen" />
              <label for="femenino1">Femenino</label>
            </div>
          </div>
        </div>
        <button className="home__btn" type="button">
          Entrar
        </button>
      </form>
    </div>
  );
}
